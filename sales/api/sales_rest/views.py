from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from .encoders import SalesPersonEncoder, CustomerEncoder, AutomobileVOEncoder, SalesEncoder
from .models import Salesperson, Sale, Customer, AutomobileVO




@require_http_methods(["GET"])
def api_automobileVO_list(request):
    if request.method == "GET":
        vos = AutomobileVO.objects.all()
        return JsonResponse(
            {"vos": vos},
            encoder=AutomobileVOEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_sales_list(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesEncoder,
            safe=False,
        )

    if request.method == "POST":
        try:
            content = json.loads(request.body)
            automobile_id = content["automobile"]
            automobilevo = AutomobileVO.objects.get(import_href=automobile_id)
            customer = Customer.objects.get(pk=content["customer"])
            salesperson = Salesperson.objects.get(pk=content["salesperson"])

            content["automobile"] = automobilevo
            content["customer"] = customer
            content["salesperson"] = salesperson

            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SalesEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the sales record"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE"])
def api_delete_sale_record(request, pk):
    count, _ = Sale.objects.filter(id=pk).delete()
    return JsonResponse({"deleted": count > 0})



@require_http_methods(["GET", "DELETE"])
def api_sales_list_by_salesperson(request, pk):
    if request.method == "GET":
        salesperson = Salesperson.objects.get(id=pk)
        sales = Sale.objects.filter(salesperson=salesperson)
        return JsonResponse(
            {"sales": sales},
            encoder=SalesEncoder,
            safe=False,
        )
    if request.method == "DELETE":
        count, _ = Salesperson.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_salesperson_list(request):
    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse(
            salespersons,
            encoder=SalesPersonEncoder,
            safe=False
        )

    if request.method == "POST":
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_customer_list(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            customers,
            encoder=CustomerEncoder,
            safe=False
        )

    if request.method == "POST":
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_delete_customer(request, pk):
    count, _ = Customer.objects.filter(id=pk).delete()
    return JsonResponse({"deleted": count > 0})
