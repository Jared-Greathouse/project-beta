from django.urls import path

from .views import (
    api_sales_list,
    api_sales_list_by_salesperson,
    api_salesperson_list,
    api_customer_list,
    api_automobileVO_list,
    api_delete_sale_record,
    api_delete_customer,
)

urlpatterns = [
    path("sales/", api_sales_list, name="api_sales_list"),
    path("sales/<int:pk>/", api_delete_sale_record, name="api_delete_sale_record"),
    path("customers/", api_customer_list, name="api_customer_list"),
    path("customers/<int:pk>/", api_delete_customer, name="api_delete_customer"),
    path("salespersons/", api_salesperson_list, name="api_salesperson_list"),
    path("salespersons/<int:pk>/", api_sales_list_by_salesperson, name="api_sales_list_by_salesperson"),
    path("automobilevos/", api_automobileVO_list, name="api_automobileVO_list"),
]
