# CarCar

Team:

* Michael - Service
* Jared - Sales

![](3978eeeefa379acf3984a32421baebac.png)


How to Run this application:
1. Locate and cd into the directory that will house Project-Beta.
2. Run this command in your terminal to pull the git project to your local machine... git clone https://gitlab.com/michaelmahurin/project-beta.git
3. Open the Docker Desktop application, then run these two commands in your terminal to get the project running...
    docker volume create beta-data
    docker-compose up --build
4. Open your browser and go to this web address... http://localhost:3000/
5. Use the navigation bar at the top of the webpage to click over to your preferred page.
6. Alternatively, open your insomnia application to test the backend RESTful API end points.

## Service microservice
Getting Started

1. Created models. (Technican, AutomobileVO, Appointment)
2. Created fully functioning poller
3. Set GET, POST, PUT, and DELETE views in views.py
4. Assigned functions to urls in urls.py
5. Tested backend with Insomnia
6. Started on the React frontend
7. Deployed forms and lists for specified requirements in Learn
8. Set up NavBar for easier access on the app
9. It works :D



Models

Technician: Technican's name + employee number.

AutomobileVO: The value object associated to the Automobile model inside the Inventory backend.
              Displays the VIN number which is the indentifier between the inventory and service
              microservices.

Appointment: Contains many attributes such as the VIN number, Customer Name, Date of appointment made,
             Reason for service, the technician assigned to that appointment, if the appointmnet is VIP
             status or not, and a tracker to check if the appointment is finished or not.


## Sales microservice
In our backend sales microservice we have 4 models (Salesperson, Customer, Sale, AutomobileVO)
We have RESTful APIs to list-all model instances in our postGreSQL database. We also have end-points to create model object instances except our AutomobileVO. We create AutomobileVO object instances through a poller that gets a list of automobiles from the Inventory microservice. The polling logic will automatically create or update automobileVO model objects to help satisfy the Sale model. The sale model demands a OneToOne relationship with an automobile with a unique VIN number in inventory. This also ensures that one cannot accidentally sell the same car twice. the front-end React App has 5 components directly tied to the sales microservice back end. These components are for forms to create a Sale, a Salesperson, and a Customer, as well as present a list of All sales, and a page to look up sale records by salesperson.
Explain your models and integration with the inventory
microservice, here.

Below is some data you can use to test the functionality of either the front end react application or the backend APIs using insomnia. Preferably, in the order outlined below

1.  Create a Manufacturer -
    Request: POST
    URL: http://localhost:8100/api/manufacturers/
    Body(JSON): paste... {"name": "Toyota"}

2.  List Manufacturers -
    Request: GET
    URL: http://localhost:8100/api/manufacturers/

3.  Create a Vehicle Model -
    Request: POST
    URL: http://localhost:8100/api/models/
    Body(JSON): paste...
    {
	"name": "Camry",
	"picture_url": "",
	"manufacturer_id": 1
    }

04. List Vehicle Models -
    Request: GET
    URL: http://localhost:8100/api/models/

05. Create an Automobile -
    Request: POST
    URL: http://localhost:8100/api/automobiles/
    Body(JSON): paste...
    {
	"color": "Magenta",
	"year": "2050",
	"vin": "01234567890123456",
    "model_id": 1
    }

06. List Automobiles -
    Request: GET
    URL: http://localhost:8100/api/automobiles/

07. Create a Salesperson -
    Request: POST
    URL: http://localhost:8090/api/salespersons/
    Body(JSON): paste...
    {
	"name": "Josh",
	"employee_number": "0613"
    }

08. List Salespersons -
    Request: GET
    URL: http://localhost:8090/api/salespersons/

09. Create a Customer -
    Request: POST
    URL: http://localhost:8090/api/customers/
    Body(JSON): paste...
    {
	"name": "Bill",
	"address": "6789 Main st Sometown, CA 94787",
	"phone_number": "5556666"
    }

10. List Customers
    Request: GET
    URL: http://localhost:8090/api/customers/

11. Create a Sale Record
    Request: POST
    URL: http://localhost:8090/api/sales/
    Body(JSON): paste...
    {
	"automobile": "/api/automobiles/01234567890123456/",
	"customer": 1,
	"salesperson": 1,
	"price": "$11,000.00"
    }

12. List All Sale Records -
    Request: GET
    URL: http://localhost:8090/api/sales/

13. Add a Technician -
    Request: POST
    URL: http://localhost:8080/api/technicians/
    Body(JSON) paste...
    {
        "name": "Michael",
        "employee_number": 12345
    }

14. List Technicians -
    Request: GET
    URL: http://localhost:8080/api/technicians/

15. Add an Appointment -
    Request: POST
    URL: http://localhost:8080/api/appointments
    Body(JSON) paste...
    {
        "vin": "01234567890123456",
        "customer_name": "Bill",
        "technician": "Michael",
        "reason_for_service": "Tire change",
        "scheduled": "2022-10-29T17:16"
    }

16. List Appointments -
    Request: GET
    URL: http://localhost:8080/api/appointments/

17. Show Service History by automobile VIN -
    Request: GET
    URL: http://localhost:8080/api/appointments/1/

18. Delete Appointment -
    Request: Delete
    URL: http://localhost:8080/api/appointments/1/

19. Delete Sale -
    Request: Delete
    URL: http://localhost:8090/api/sales/1/

20. Delete Customer -
    Request: Delete
    URL: http://localhost:8090/api/customers/1/

21. Delete Salesperson -
    Request: Delete
    URL: http://localhost:8090/api/salespersons/1/

22. Delete Automobile -
    Request: Delete
    URL: http://localhost:8100/api/automobiles/01234567890123456/

23. Delete Vehicle Model -
    Request: Delete
    URL: http://localhost:8100/api/models/1/

24. Delete Manufacturer -
    Request: Delete
    URL: http://localhost:8100/api/manufacturers/1/
