import React from "react";
class CreateASalesRecordForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            automobiles: [],
            salespersons: [],
            customers: [],
            price: "",
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
        this.handleSalesPersonChange = this.handleSalesPersonChange.bind(this);
        this.handleCustomerChange = this.handleCustomerChange.bind(this);
        this.handlePriceChange = this.handlePriceChange.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}
        delete data.automobiles;
        delete data.salespersons;
        delete data.customers;
        const SalesRecordURL = "http://localhost:8090/api/sales/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(SalesRecordURL, fetchConfig);
        if (response.ok) {
            const newSalesRecord = await response.json();

            const cleared = {
                automobile: "",
                salesperson: "",
                customer: "",
                price: ""
            };
            this.setState(cleared);
        }
    }
    handleAutomobileChange(event) {
        const value = event.target.value;
        this.setState({ automobile: value })
    }
    handleSalesPersonChange(event) {
        const value = event.target.value;
        this.setState({ salesperson: value })
    }
    handleCustomerChange(event) {
        const value = event.target.value;
        this.setState({ customer: value })
    }
    handlePriceChange(event) {
        const value = event.target.value;
        this.setState({ price: value })
    }
    async componentDidMount() {
        const Salespersonurl = 'http://localhost:8090/api/salespersons/';
        const responseSales = await fetch(Salespersonurl);
        if (responseSales.ok) {
            const data = await responseSales.json();
            this.setState({ "salespersons": data });
        };

        const Customerurl = 'http://localhost:8090/api/customers/';
        const responseCustomer = await fetch(Customerurl);
        if (responseCustomer.ok) {
            const data = await responseCustomer.json();
            this.setState({ "customers": data });
        };

        const Automobileurl = 'http://localhost:8100/api/automobiles/';
        const responseAuto = await fetch(Automobileurl);
        if (responseAuto.ok) {
            const data = await responseAuto.json();
            this.setState({ "automobiles": data.autos });
        };
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new sales record</h1>
                        <form
                            onSubmit={this.handleSubmit}
                            id="create-salesrecord-form"
                        >
                            <div className="mb-3">
                                <select
                                    className="form-select"
                                    name="automobile"
                                    id="automobile"
                                    required
                                    defaultValue=""
                                    value={this.state.automobile}
                                    onChange={this.handleAutomobileChange}
                                >
                                    <option>Choose an automobile</option>
                                    {this.state.automobiles.map(automobile => {
                                        return (
                                            <option
                                                key={automobile.href}
                                                value={automobile.href}
                                            >
                                            {automobile.vin}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select
                                    className="form-select"
                                    name="salesperson"
                                    required
                                    id="salesperson"
                                    defaultValue=""
                                    value={this.state.salesperson}
                                    onChange={this.handleSalesPersonChange}
                                >
                                    <option>Choose a Salesperson</option>
                                    {this.state.salespersons.map(salesperson => {
                                        return (
                                            <option
                                                key={salesperson.id}
                                                value={salesperson.id}
                                            >
                                            {salesperson.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select
                                    className="form-select"
                                    name="customer"
                                    id="customer"
                                    required
                                    defaultValue=""
                                    value={this.state.customer}
                                    onChange={this.handleCustomerChange}
                                >
                                    <option>Choose a Customer</option>
                                    {this.state.customers.map(customer => {
                                        return (
                                            <option
                                                key={customer.id}
                                                value={customer.id}
                                            >
                                            {customer.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    className="form-control"
                                    type="number"
                                    name="price"
                                    id="price"
                                    placeholder="price"
                                    required
                                    value={this.state.price}
                                    onChange={this.handlePriceChange}
                                />
                                <label htmlFor="price">Price</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}
export default CreateASalesRecordForm;
