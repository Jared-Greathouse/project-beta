import React, { useState, useEffect } from "react";
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AddASalesPersonForm from './AddASalesPersonForm';
import AddAPotentialCustomerForm from './AddAPotentialCustomerForm';
import CreateASalesRecordForm from './CreateASalesRecordForm';
import ListOfSalesBySalesPerson from './ListOfSalesBySalesPerson';
import ListOfAllSales from './ListOfAllSales';
import VehicleModelList from './VehicleModelList';
import CreateVehicleModelForm from './CreateVehicleModelForm';
import CreateAutomobileForm from './CreateAutomobileForm';
import AppointmentForm from './AppointmentForm';
import TechnicianForm from './TechnicianForm';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import AutomobileList from './AutomobileList';


function App(props) {
  return (
      <BrowserRouter>
      <Nav />
      <div className="container">
          <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="salespersons">
              <Route path="new" element={<AddASalesPersonForm />}/>
              <Route path="list" element={<ListOfSalesBySalesPerson />}/>
          </Route>
          <Route path="customers">
              <Route path="new" element={<AddAPotentialCustomerForm />}/>
          </Route>
          <Route path="sales">
              <Route path="new" element={<CreateASalesRecordForm />}/>
              <Route path="" element={<ListOfAllSales />}/>
          </Route>
          <Route path="models">
              <Route path="new" element={<CreateVehicleModelForm />}/>
              <Route path="" element={<VehicleModelList />}/>
          </Route>
          <Route path="automobiles">
              <Route path="new" element={<CreateAutomobileForm />}/>
              <Route path="list" element={<AutomobileList />}/>
          </Route>
          <Route path="technician">
              <Route path="new" element={<TechnicianForm />}/>
          </Route>
          <Route path="manufacturer">
              <Route path="new" element={<ManufacturerForm/>}/>
              <Route path="list" element={<ManufacturerList />}/>
          </Route>
          <Route path="service">
              <Route path= "" element={<ServiceHistory />}/>
          </Route>
          <Route path="appointment">
              <Route path="new" element={<AppointmentForm />}/>
              <Route path="list" element={<AppointmentList />}/>
          </Route>

          </Routes>
      </div>
      </BrowserRouter>
  );
}

export default App;
