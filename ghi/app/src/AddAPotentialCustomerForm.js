import React from "react";
class AddAPotentialCustomerForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            name: "",
            address: "",
            phone_number: ""
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleAddressChange = this.handleAddressChange.bind(this);
        this.handlePhoneNumberChange = this.handlePhoneNumberChange.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}
        const CustomerURL = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(CustomerURL, fetchConfig);
        if (response.ok) {

            const cleared = {
                name: "",
                address: "",
                phone_number: ""
            };

            this.setState(cleared);
        }
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }
    handleAddressChange(event) {
        const value = event.target.value;
        this.setState({ address: value })
    }
    handlePhoneNumberChange(event) {
        const value = event.target.value;
        this.setState({ phone_number: value })
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new customer</h1>
                        <form
                            onSubmit={this.handleSubmit}
                            id="create-customer-form"
                        >
                            <div className="form-floating mb-3">
                                <input
                                    className="form-control"
                                    type="text"
                                    name="name"
                                    id="name"
                                    placeholder="name"
                                    required
                                    value={this.state.name}
                                    onChange={this.handleNameChange}
                                />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    className="form-control"
                                    type="text"
                                    name="address"
                                    id="address"
                                    placeholder="address"
                                    required
                                    value={this.state.address}
                                    onChange={this.handleAddressChange}
                                />
                                <label htmlFor="address">Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    className="form-control"
                                    type="text"
                                    name="phone_number"
                                    id="phone_number"
                                    placeholder="phone_number"
                                    required
                                    value={this.state.phone_number}
                                    onChange={this.handlePhoneNumberChange}
                                />
                                <label htmlFor="phone_number">7-Digit Phone Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}
export default AddAPotentialCustomerForm;
