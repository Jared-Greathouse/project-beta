import React from "react";

class VehicleModelList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            models: [],
        }
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            this.setState({ "models": data.models });
        };

    }
    render() {
        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Picture_url</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.models.map((model, index) => {
                        return (
                            <tr key={index}>
                                <td>{model.name}</td>
                                <td>{model.picture_url}</td>
                                <td>{model.manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        );
    }
}

export default VehicleModelList;
