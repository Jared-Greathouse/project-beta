import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-black">
      <div className="container-fluid">
        <a className="navbar-brand" href="#"></a>
        <NavLink className="navbar-brand" to="/">
          CarCar
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNavDarkDropdown"
          aria-controls="navbarNavDarkDropdown"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
          <ul className="navbar-nav">
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="navbarDarkDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Inventory
              </a>
              <ul
                className="dropdown-menu dropdown-menu-dark"
                aria-labelledby="navbarDarkDropdownMenuLink"
              >
                <li>
                  <NavLink
                    className="dropdown-item"
                    to="manufacturer/list"
                  >
                    Manufacturers
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    className="dropdown-item"
                    to="manufacturer/new"
                  >
                    Add Manufacturer
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    className="dropdown-item"
                    to="models"
                  >
                    Vehicle Models
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    className="dropdown-item"
                    to="models/new"
                  >
                    Add Vehicle Models
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    className="dropdown-item"
                    to="automobiles/list"
                  >
                    Automobiles
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    className="dropdown-item"
                    to="automobiles/new"
                  >
                    Add Automobile
                  </NavLink>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="navbarDarkDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Sales
              </a>
              <ul
                className="dropdown-menu dropdown-menu-dark"
                aria-labelledby="navbarDarkDropdownMenuLink"
              >
                <li>
                  <NavLink className="dropdown-item" to="customers/new">
                    Add Customer
                  </NavLink>
                </li>

                <li>
                  <NavLink className="dropdown-item" to="salespersons/new">
                    Add Salesperson
                  </NavLink>
                </li>

                <li>
                  <NavLink className="dropdown-item" to="sales/new">
                    Create Record
                  </NavLink>
                </li>

                <li>
                  <NavLink className="dropdown-item" to="sales">
                    List of Sales
                  </NavLink>
                </li>

                <li>
                  <NavLink className="dropdown-item" to="salespersons/list">
                    Sales Persons History
                  </NavLink>
                </li>
              </ul>
            </li>

            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                href="#"
                id="navbarDarkDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Service
              </a>
              <ul
                className="dropdown-menu dropdown-menu-dark"
                aria-labelledby="navbarDarkDropdownMenuLink"
              >
                <li>
                  <NavLink className="dropdown-item" to="technician/new">
                    Add Technician
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    className="dropdown-item"
                    to="appointment/new"
                  >
                    Add Appointment
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    className="dropdown-item"
                    to="appointment/list"
                  >
                    Show Appointments
                  </NavLink>
                </li>

                <li>
                  <NavLink
                    className="dropdown-item"
                    to="service"
                  >
                    Service History
                  </NavLink>
                </li>

              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
