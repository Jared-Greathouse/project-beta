import React from "react";

class CreateAutomobileForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            color: "",
            year: "",
            vin: "",
            models: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleYearChange = this.handleYearChange.bind(this);
        this.handleVinChange = this.handleVinChange.bind(this);
        this.handleModelIDChange = this.handleModelIDChange.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.models;
        const autoURL = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(autoURL, fetchConfig);
        if (response.ok) {

            const cleared = {
                color: "",
                year: "",
                vin: "",
                model_id: "",
            };
            this.setState(cleared);
        }
    }
    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value })
    }
    handleYearChange(event) {
        const value = event.target.value;
        this.setState({ year: value })
    }
    handleVinChange(event) {
        const value = event.target.value;
        this.setState({ vin: value })
    }
    handleModelIDChange(event) {
        const value = event.target.value;
        this.setState({ model_id: value })
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ "models": data.models });
        };
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new Automobile</h1>
                        <form
                            onSubmit={this.handleSubmit}
                            id="create-auto-form"
                        >
                            <div className="form-floating mb-3">
                                <input
                                    className="form-control"
                                    type="text"
                                    name="color"
                                    id="color"
                                    placeholder="color"
                                    required
                                    value={this.state.color}
                                    onChange={this.handleColorChange}
                                />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    className="form-control"
                                    type="text"
                                    name="year"
                                    id="year"
                                    placeholder="year"
                                    required
                                    value={this.state.year}
                                    onChange={this.handleYearChange}
                                />
                                <label htmlFor="year">Year</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    className="form-control"
                                    type="text"
                                    name="vin"
                                    id="vin"
                                    placeholder="vin"
                                    required
                                    value={this.state.vin}
                                    onChange={this.handleVinChange}
                                />
                                <label htmlFor="vin">Vin</label>
                            </div>
                            <div className="mb-3">
                                <select
                                    className="form-select"
                                    name="model_id"
                                    id="model_id"
                                    required
                                    defaultValue=""
                                    onChange={this.handleModelIDChange}
                                >
                                    <option>Choose a Vehicle Model</option>
                                    {this.state.models.map(model_id => {
                                        return (
                                            <option
                                                key={model_id.id}
                                                value={model_id.id}
                                            >
                                            {model_id.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

}
export default CreateAutomobileForm;
